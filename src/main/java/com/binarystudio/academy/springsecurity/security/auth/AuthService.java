package com.binarystudio.academy.springsecurity.security.auth;

import com.binarystudio.academy.springsecurity.domain.user.UserService;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.security.auth.model.*;
import com.binarystudio.academy.springsecurity.security.jwt.JwtProvider;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class AuthService {
	private final UserService userService;
	private final JwtProvider jwtProvider;
	private final PasswordEncoder passwordEncoder;

	public AuthService(UserService userService, JwtProvider jwtProvider, PasswordEncoder passwordEncoder) {
		this.userService = userService;
		this.jwtProvider = jwtProvider;
		this.passwordEncoder = passwordEncoder;
	}

	public AuthResponse performLogin(AuthorizationRequest authorizationRequest) {
		var userDetails = userService.loadUserByUsername(authorizationRequest.getUsername());
		if (passwordsDontMatch(authorizationRequest.getPassword(), userDetails.getPassword())) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid password");
		}
		// 2. todo: auth and refresh token are given to user
		return AuthResponse.of(jwtProvider.generateAccessToken(userDetails));
	}

	private boolean passwordsDontMatch(String rawPw, String encodedPw) {
		return !passwordEncoder.matches(rawPw, encodedPw);
	}

    public AuthResponse register(RegistrationRequest registrationRequest) {
        var foundUserByLogin = userService.findUserByLogin(registrationRequest.getLogin());
        var foundUserByEmail = userService.findUserByEmail(registrationRequest.getEmail());

        if (foundUserByEmail.isPresent()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "This email already register.");
        } else if (foundUserByLogin.isPresent()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "This login already exist.");
        }
        var createdUser = userService.register(registrationRequest.getEmail(), registrationRequest.getLogin(), passwordEncoder.encode(registrationRequest.getPassword()));

        return AuthResponse.of(jwtProvider.generateAccessToken(createdUser));
    }

    public void forgotPasswordRequest(String email) {
        Optional<User> user = userService.findUserByEmail(email);

        if (user.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not found user with this email.");
        }
        System.out.println(AuthResponse.of(jwtProvider.generateAccessToken(user.get())));
    }

    public AuthResponse forgottenPasswordReplacement(ForgottenPasswordReplacementRequest forgottenPasswordReplacementRequest) {
        String userLogin = jwtProvider.getLoginFromToken(forgottenPasswordReplacementRequest.getToken());
        var userDetails = userService.loadUserByUsername(userLogin);
        userDetails.setPassword(forgottenPasswordReplacementRequest.getNewPassword());
        userService.updateUserPassword(userDetails);

        return AuthResponse.of(jwtProvider.generateAccessToken(userDetails));
    }

    public AuthResponse changePassword(PasswordChangeRequest passwordChangeRequest, User user) {
        user.setPassword(passwordEncoder.encode(passwordChangeRequest.getNewPassword()));
        return AuthResponse.of(jwtProvider.generateAccessToken(user));
    }
}
